* got 0.17; 2019-10-06
- make 'got branch' without args show work tree's branch (requested by benno)
- show temporary branch in 'got branch -l' during rebase and histedit
- refresh references when 'tog log' view is refreshed with Ctrl+L
- make 'got status' read .gitignore files; support **/ and /**/ in patterns
- handle empty trees during 'import' and 'checkout' (issue reported by sthen)
- show only whitelisted commit headers in 'got log' (semarie)
- properly handle getline(3) return type as ssize_t (hiltjo)

* got 0.16; 2019-09-23
- replace fgetln(3) with getline(3) for portability (patch by Steven McDonald)
- fix implicit time.h includes (patch by Steven McDonald)
- fix tiny memory leaks and other minor bugs found by jj@ with scan-build
- fix missing error checks (found by jj@ with scan-build and by Steven McDonald)
- avoid a couple of potential NULL derefs (found by jj@ with scan-build)
- fix cosmetic display issues involving \n and TABs in tog(1)

* got 0.15; 2019-09-08
- use author name/email from ~/.gitconfig if GOT_AUTHOR is not set
- obtain repo format version and commit author name/email from .git/config
- fix line-wrap for lines spanning the terminal in tog diff view
- make 'got status' ignore inaccessible directories (reported by semarie)
- unstage may need to write to repository; fix unveil(2) call accordingly
- fix modified files not shown by 'got status' after committing staged changes

* got 0.14; 2019-09-03
- fix tag_list test failure on single-day-digit dates (patch by Evan Silberman)
- prevent theoretical double-free with non-OpenBSD asprintf (found by jasper@)
- fix NULL deref in got_error_from_errno via got_error_uuid (found by jasper@)
- make tog release its work tree lock before going into main loop
- 'got stage' writes to repository, fix unveil(2) call accordingly

* got 0.13; 2019-08-30
- fix several possible leaks, use after frees, and NULL derefs (hiltjo)
- tog: mbs2ws: fix sizeof of element for memory allocation (hiltjo)
- fix deletion of branches stored in packed-refs
- fix 'got log path' in a work tree with a path prefix (with Evan Silberman)

* got 0.12; 2019-08-26
- add support for path arguments to 'got cat'
- make 'got log -p path' display patches for the specified path only
- allow creating branches based on commit IDs
- prevent deletion of the work tree's current branch
- detect and ignore Git submodules
- indicate non-existent path arguments in 'got status'
- make 'got diff' error for non-existent path arguments
- make 'got tag -l' list tags by time stamp in descending order
- fix reading pack index files with large offsets (patch by Hiltjo Posthuma)

* got 0.11; 2019-08-23
- add 'got tag' command
- add 'got cat' command
- support quick cancellation in 'tog blame' view
- fix search prompt location in split-screen tog views

* got 0.10; 2019-08-21
- fix and simplify blame algorithm

* got 0.9; 2019-08-19
- fix blame bug where lines were annotated with wrong commit
- man page spelling fixes (patch by Hiltjo Posthuma)
- fix uninitialized 'editor' variable (found by Hiltjo Posthuma)
- make 'got revert' ignore unversioned files instead of aborting (semarie@)
- fix "last commit cannot be folded" histedit check with reordered commits

* got 0.8; 2019-08-16
- write tree entries the way git likes it to prevent git index corruption
- make 'got blame' show line numbers, dates, and author names
- add GOT_LOG_DEFAULT_LIMIT env var for setting a got log -l default

* got 0.7; 2019-08-13
- properly parse timestamps in commit objects as UTC (patch by Hiltjo Posthuma)
- strip Git GPG signatures from log messages
- fix regression: don't try to parse "leightweight" tags as real tag objects

* got 0.6; 2019-08-13
- 'got log' and 'tog log' now display tags in addition to branch references
- 'got ref' can now be used to create symbolic references
- use <limits.h> instead of <sys/limits.h> (patch by Thomas Klausner)
- fix crash if 'got rebase' has no commits to rebase (with semarie@)
- annotate symlinks with @ in 'got tree' and 'tog tree'

* got 0.5; 2019-08-12
- fix blame with single-commit history and with files without \n
- fix crashes in tog when blame returns no results on a file (found by otto@)
- man page improvements (bentley@)

* got 0.4; 2019-08-11
- add support for tag objects to 'got diff'
- fix tog log -r option (patch by semarie@)
- fix use of potentially out-of-date errno values (patch by Hiltjo Posthuma)
- fix getline(3) return value check (patch by Hiltjo Posthuma)
- use Oo/Oc/Op instead of plain brackets in man pages (patch by bentley@)
- fix spurious failures of test_revert_patch_one_change due to race condition

* got 0.3; 2019-08-10
- fix bug where 'revert -p' would delete all lines following a reverted change
- fix merge bug inherited from OpenRCS, affecting lines with leading .
- fix man page installation during manual 'make install' from plain tarball

* got 0.2; 2019-08-09
  - improve error message if helper binaries fail to unveil(2)
  - add support for tags to -c options of some got commands
  - attempt to reject GOT_AUTHOR values without an email address because git
    is unable to parse resulting commit objects
  - fix build on OpenBSD/sparc64 (gcc)

* got 0.1; 2019-08-09
  initial release; see git repository history for changes up to this point
